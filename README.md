Just a website made with Vue.js where you can get information on your favorite bike stations in New York. 
Made with the City Bikes API (http://api.citybik.es/v2/).

# projet-web

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
